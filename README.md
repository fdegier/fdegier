## Fred's README

**Fred de Gier - Staff Backend Engineer** This page is intended to help others understand what it might be like to work with me, especially people who haven’t worked with me before. 

It’s also a well-intentioned effort at building some trust by being intentionally vulnerable, and to share my ideas of a good working relationship to reduce the anxiety of people who might be on my team.

Please feel free to contribute to this page by opening a merge request. 

## Related pages
- MLOps https://handbook.gitlab.com/handbook/engineering/development/data-science/modelops/mlops/

Previously I was working as a Staff Incubation Engineer on the following:
- AI Assist SEG https://about.gitlab.com/handbook/engineering/incubation/ai-assist/
- Airflow SEG https://about.gitlab.com/handbook/engineering/incubation/airflow/

## About me
My non work related hobbies / interests are (in no particular order) gardening :tulip:, hiking :runner:, DIY projects :wrench:, home automation, lego, star-wars :milky_way:, series :tv:, gaming (PCMR) :video_game:, bbqing :hamburger:. I speak Dutch, English, German, French and am currently learning Spanish.

Since 2012 I have been working in the field of big data and machine learning for various companies, branches and in different roles. Mostly building applications that deliver sustainable value from data. I’m very interested in CI/CD, containerization, automation, dark data, creating tools that simplify things, creating frameworks for efficiency and achieving results.

## How you can help me
- If you have anything to share, do reach out
- Provide honest [feedback](https://about.gitlab.com/handbook/people-group/guidance-on-feedback/) directly 1:1
- Read the documenation first
- When in doubt, overcommunicate

## My working style
- [Iteration](https://about.gitlab.com/handbook/values/#iteration) is what I try to apply at everything work related. 
- I always strived for [MVP's](https://www.productplan.com/glossary/minimum-viable-product/) at GitLab it goes 1 step further Minimal Viable Change (MVC), I intend to adopt that.
- I strive to write code as clean and simple as possible. If I haven't looked at the code for 6 months and there is an urgent issue, it should not take me or my team members a lot of effort to decipher the code.
- I prefer to take my time refining ideas, concepts and work before actually writing code. This often involves talking with other people. And leads to increased efficiency.

## What I assume about others
I [Assume positive intent](https://about.gitlab.com/handbook/values/#assume-positive-intent) and honesty

## Communicating with me
- I fully embrace [async communication](https://about.gitlab.com/company/culture/all-remote/asynchronous/), whenever you want to communicate with me, just open a MR and tag me or send a message on slack `@fdegier`
- If a Zoom video call is required, I prefer to have those on Tuesdays, Wednesdays and Thursdays between 09:00 and 16:00 AMS. External parties can schedule a meeting via [Calendly](https://calendly.com/fdegier/45min)
- Always open for coffee chats!
- I try my best to be aware of my Dutch bias and communication style, for more information about Dutch verbal communication see https://culturalatlas.sbs.com.au/dutch-culture/dutch-culture-communication

## Strengths/Weaknesses
:muscle:
- Innovative, creative and entrepreneurial spirit
- When I don't know something I will learn it
- Always willing to help others

:thumbsdown:
- Small details can distract me, I need a clean desk / office to concentrate
- I can be too honest and not care about office politics

## Remote setup
- LG34WK95U-W 5k ultra wide monitor
- Apple MacBook 16" M3 Max 36gb
- Apple Magic Keyboard Touch ID
- Apple Magic Mouse
- Apple Magic Trackpad _[not-reimbursed]_
- Herman Miller Embody chair
- Logitech C920 Webcam
- Chief K1C monitor arm _[not-reimbursed]_
